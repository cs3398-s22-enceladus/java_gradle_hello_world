package hello;

// on line change
//Assignment 14 edit

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


// Starting class (3-4)
// Jordan's comment - A14

public class TestGreeter {

   private Greeter g = new Greeter();

   @Test
   @DisplayName("Test for Empty Name")
   public void testGreeterEmpty() 

   {
      assertEquals(g.getName(),"");
      assertEquals(g.sayHello(),"Hello!");
   }

   

    @Test
   @DisplayName("Test for Jordan")
   public void testGreeterJordan() 

   {
      g.setName("Jordan");
      assertEquals(g.getName(),"Jordan");
      assertEquals(g.sayHello(),"Hello Jordan!");
   }




   @Test
   @DisplayName("Test for Zechariah Johnson")
   public void testGreeterZechariah() 

   {
      g.setName("Zechariah Johnson");
      assertEquals(g.getName(),"Zechariah Johnson");
      assertEquals(g.sayHello(),"Hello Zechariah Johnson!");
   }



   @Test
   @DisplayName("Test for Ertugrul Oksuz")
   public void testGreeterErtugrul() 

   {
      g.setName("Ertugrul Oksuz");
      assertEquals(g.getName(),"Ertugrul Oksuz");
      assertEquals(g.sayHello(),"Hello Ertugrul Oksuz!");
   }

   @Test
   @DisplayName("Test for Kaizhao Cheng")
   public void testGreeterKai() 

   {
      g.setName("Kaizhao Cheng");
      assertEquals(g.getName(),"Kaizhao Cheng");
      assertEquals(g.sayHello(),"Hello Kaizhao Cheng!");
   }

   

   @Test
   @DisplayName("Test for Name='World'")
   public void testGreeter() 
   {

      g.setName("World");
      assertEquals(g.getName(),"World");
      assertEquals(g.sayHello(),"Hello World!");
   }

}
