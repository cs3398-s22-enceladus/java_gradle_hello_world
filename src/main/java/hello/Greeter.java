package hello;

// this is the comment 
// z_j29 comment
//ertugrul Oksuz making a change

public class Greeter {

  private String name = "";


  public String getName() 

  {
    return name;
  }


  public void setName(String name) 

  {
      this.name = name;
  }


  public String sayHello() 

  {
  	if (name == "") 

    {
       return "Hello!";
    }
    else 
    {
       return "Hello " + name + "!";
    }

  }

}
